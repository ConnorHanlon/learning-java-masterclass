# learning-java-masterclass

The code in this repository is a result of walking through the Udemy course `Java the Complete Java Developer Course` by `Tim Buchalka`.

# Notes

## Tools

### JShell

REPL (read, execute, print, loop) program useful for quickly developing and running Java code.
Documentation: https://docs.oracle.com/en/java/javase/20/jshell/introduction-jshell.html

Commands:

- Run with `jshell`
- Help `/help`
- List command history `/list -all`
